using Microsoft.EntityFrameworkCore;
// modules
using Main;


internal class Program
{
    private static void Main(String[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.Services.AddDbContext<ContextDb>(opt => opt.UseInMemoryDatabase("db"));
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        var app = builder.Build();

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        //mapgroups:
        var main = app.MapGroup("/Main");

        var projeto = main.MapGroup("/Projeto");
        projeto.MapGet("/", async (ContextDb db) =>
            await db.Projetos.ToListAsync());
        projeto.MapGet("/{id}", async (int id, ContextDb db) =>
            await db.Projetos.FindAsync(id)
                is Projeto projeto
                ? Results.Ok(projeto)
                : Results.NotFound());

        projeto.MapPost("/", async (Projeto projeto, ContextDb db) =>
        {
            db.Projetos.Add(projeto);
            await db.SaveChangesAsync();
            return Results.Created($"/projeto/{projeto.Id}", projeto);
        });

        projeto.MapPut("/{id}", async (int id, Projeto inputProjeto, ContextDb db) =>
        {
            var projeto = await db.Projetos.FindAsync(id);
            if (projeto is null) return Results.NotFound();
                projeto.Nome = inputProjeto.Nome;
                projeto.Data_inicio = inputProjeto.Data_inicio;
                projeto.Data_fim = inputProjeto.Data_fim;
                projeto.Orcamento = inputProjeto.Orcamento;
                projeto.Cliente = inputProjeto.Cliente;
                projeto.Patrocinador = inputProjeto.Patrocinador;
                projeto.Objetivo = inputProjeto.Objetivo;
            await db.SaveChangesAsync();
            return Results.NoContent();
        });

        projeto.MapDelete("/{id}", async (int id, ContextDb db) =>
        {
            if (await db.Projetos.FindAsync(id) is Projeto projeto) {
                db.Projetos.Remove(projeto);
                await db.SaveChangesAsync();
                return Results.NoContent();
            }

         return Results.NotFound();
        });

        var categoria = main.MapGroup("/Categoria");
        categoria.MapGet("/", async (ContextDb db) =>
            await db.Categorias.ToListAsync());
        categoria.MapGet("/{id}", async (int id, ContextDb db) =>
            await db.Categorias.FindAsync(id)
                is Categoria categoria
                ? Results.Ok(categoria)
                : Results.NotFound());

        categoria.MapPost("/", async (Categoria categoria, ContextDb db) =>
        {
            db.Categorias.Add(categoria);
            await db.SaveChangesAsync();
            return Results.Created($"/categoria/{categoria.Id}", categoria);
        });

        categoria.MapPut("/{id}", async (int id, Categoria inputCategoria, ContextDb db) =>
        {
            var categoria = await db.Categorias.FindAsync(id);
            if (categoria is null) return Results.NotFound();
                categoria.Nome = inputCategoria.Nome;
            await db.SaveChangesAsync();
            return Results.NoContent();
        });

        categoria.MapDelete("/{id}", async (int id, ContextDb db) =>
        {
            if (await db.Categorias.FindAsync(id) is Categoria categoria) {
                db.Categorias.Remove(categoria);
                await db.SaveChangesAsync();
                return Results.NoContent();
            }

         return Results.NotFound();
        });

        var time = main.MapGroup("/Time");
        time.MapGet("/", async (ContextDb db) =>
            await db.Times.ToListAsync());
        time.MapGet("/{id}", async (int id, ContextDb db) =>
            await db.Times.FindAsync(id)
                is Time time
                ? Results.Ok(time)
                : Results.NotFound());

        time.MapPost("/", async (Time time, ContextDb db) =>
        {
            db.Times.Add(time);
            await db.SaveChangesAsync();
            return Results.Created($"/time/{time.Id}", time);
        });

        time.MapPut("/{id}", async (int id, Time inputTime, ContextDb db) =>
        {
            var time = await db.Times.FindAsync(id);
            if (time is null) return Results.NotFound();
                time.Nome = inputTime.Nome;
                time.Objetivo = inputTime.Objetivo;
            await db.SaveChangesAsync();
            return Results.NoContent();
        });

        time.MapDelete("/{id}", async (int id, ContextDb db) =>
        {
            if (await db.Times.FindAsync(id) is Time time) {
                db.Times.Remove(time);
                await db.SaveChangesAsync();
                return Results.NoContent();
            }

         return Results.NotFound();
        });

        var alocacao = main.MapGroup("/Alocacao");
        alocacao.MapGet("/", async (ContextDb db) =>
            await db.Alocacaos.ToListAsync());
        alocacao.MapGet("/{id}", async (int id, ContextDb db) =>
            await db.Alocacaos.FindAsync(id)
                is Alocacao alocacao
                ? Results.Ok(alocacao)
                : Results.NotFound());

        alocacao.MapPost("/", async (Alocacao alocacao, ContextDb db) =>
        {
            db.Alocacaos.Add(alocacao);
            await db.SaveChangesAsync();
            return Results.Created($"/alocacao/{alocacao.Id}", alocacao);
        });

        alocacao.MapPut("/{id}", async (int id, Alocacao inputAlocacao, ContextDb db) =>
        {
            var alocacao = await db.Alocacaos.FindAsync(id);
            if (alocacao is null) return Results.NotFound();
                alocacao.Data_inicio = inputAlocacao.Data_inicio;
                alocacao.Data_final = inputAlocacao.Data_final;
                alocacao.Carga_horaria = inputAlocacao.Carga_horaria;
            await db.SaveChangesAsync();
            return Results.NoContent();
        });

        alocacao.MapDelete("/{id}", async (int id, ContextDb db) =>
        {
            if (await db.Alocacaos.FindAsync(id) is Alocacao alocacao) {
                db.Alocacaos.Remove(alocacao);
                await db.SaveChangesAsync();
                return Results.NoContent();
            }

         return Results.NotFound();
        });

        var membro = main.MapGroup("/Membro");
        membro.MapGet("/", async (ContextDb db) =>
            await db.Membros.ToListAsync());
        membro.MapGet("/{id}", async (int id, ContextDb db) =>
            await db.Membros.FindAsync(id)
                is Membro membro
                ? Results.Ok(membro)
                : Results.NotFound());

        membro.MapPost("/", async (Membro membro, ContextDb db) =>
        {
            db.Membros.Add(membro);
            await db.SaveChangesAsync();
            return Results.Created($"/membro/{membro.Id}", membro);
        });

        membro.MapPut("/{id}", async (int id, Membro inputMembro, ContextDb db) =>
        {
            var membro = await db.Membros.FindAsync(id);
            if (membro is null) return Results.NotFound();
                membro.Nome = inputMembro.Nome;
                membro.Email_x = inputMembro.Email_x;
                membro.Matricula = inputMembro.Matricula;
                membro.Cpf_x = inputMembro.Cpf_x;
                membro.Cep = inputMembro.Cep;
                membro.Telefone = inputMembro.Telefone;
            await db.SaveChangesAsync();
            return Results.NoContent();
        });

        membro.MapDelete("/{id}", async (int id, ContextDb db) =>
        {
            if (await db.Membros.FindAsync(id) is Membro membro) {
                db.Membros.Remove(membro);
                await db.SaveChangesAsync();
                return Results.NoContent();
            }

         return Results.NotFound();
        });

        var funcao = main.MapGroup("/Funcao");
        funcao.MapGet("/", async (ContextDb db) =>
            await db.Funcaos.ToListAsync());
        funcao.MapGet("/{id}", async (int id, ContextDb db) =>
            await db.Funcaos.FindAsync(id)
                is Funcao funcao
                ? Results.Ok(funcao)
                : Results.NotFound());

        funcao.MapPost("/", async (Funcao funcao, ContextDb db) =>
        {
            db.Funcaos.Add(funcao);
            await db.SaveChangesAsync();
            return Results.Created($"/funcao/{funcao.Id}", funcao);
        });

        funcao.MapPut("/{id}", async (int id, Funcao inputFuncao, ContextDb db) =>
        {
            var funcao = await db.Funcaos.FindAsync(id);
            if (funcao is null) return Results.NotFound();
                funcao.Nome = inputFuncao.Nome;
            await db.SaveChangesAsync();
            return Results.NoContent();
        });

        funcao.MapDelete("/{id}", async (int id, ContextDb db) =>
        {
            if (await db.Funcaos.FindAsync(id) is Funcao funcao) {
                db.Funcaos.Remove(funcao);
                await db.SaveChangesAsync();
                return Results.NoContent();
            }

         return Results.NotFound();
        });



        app.MapGet("/", () => "Hello World!");
        app.Run();
    }
}
