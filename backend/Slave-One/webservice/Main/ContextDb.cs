namespace Main{

using Microsoft.EntityFrameworkCore;

internal class ContextDb : DbContext
    {
        public ContextDb(DbContextOptions<ContextDb> options) : base(options) { }
        public DbSet<Projeto> Projetos { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Time> Times { get; set; }
        public DbSet<Alocacao> Alocacaos { get; set; }
        public DbSet<Membro> Membros { get; set; }
        public DbSet<Funcao> Funcaos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        modelBuilder.Entity<Projeto>()
            .HasMany(projeto => projeto.Projetos)
            .WithOne(projeto => projeto.ProjetoPai)
            .HasForeignKey(projeto => projeto.projetoId);

        modelBuilder.Entity<Projeto>()
            .HasMany(time => time.Times)
            .WithOne(time => time.Projeto)
            .HasForeignKey(projeto => projeto.projetoId);


        modelBuilder.Entity<Categoria>()
            .HasMany(projeto => projeto.Projetos)
            .WithOne(projeto => projeto.Categoria)
            .HasForeignKey(categoria => categoria.categoriaId);


        modelBuilder.Entity<Time>()
            .HasMany(alocacao => alocacao.Alocacaos)
            .WithOne(alocacao => alocacao.Time)
            .HasForeignKey(time => time.timeId);



        //ManyToMany
        modelBuilder.Entity<Alocacao>()
          .HasMany(funcao => funcao.Funcaos)
          .WithMany(funcao => funcao.Alocacaos);

        modelBuilder.Entity<Membro>()
            .HasMany(alocacao => alocacao.Alocacaos)
            .WithOne(alocacao => alocacao.Membro)
            .HasForeignKey(membro => membro.membroId);



        }
    }
}
