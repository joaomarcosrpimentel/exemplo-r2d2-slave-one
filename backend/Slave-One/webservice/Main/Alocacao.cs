namespace Main
{
  using Microsoft.EntityFrameworkCore;



public  class Alocacao  {

  private DateTime createdAt = DateTime.Now;

  public Guid Id { get; set; }


  public DateTime Data_inicio { get; set; }


  public DateTime Data_final { get; set; }


  public decimal Carga_horaria { get; set; }

  //ManyToOne
  public Membro? Membro { get; set; }
  public Guid? membroId {get; set; }

  //ManyToOne
  public Time? Time { get; set; }
  public Guid? timeId {get; set; }

  //ManyToMany
  public ICollection<Funcao>? Funcaos { get; set;}


  public AlocacaoStatus alocacaostatus { get; set; }


}
}
