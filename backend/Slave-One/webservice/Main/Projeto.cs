namespace Main
{
  using Microsoft.EntityFrameworkCore;



public  class Projeto  {

  private DateTime createdAt = DateTime.Now;

  public Guid Id { get; set; }


  public string Nome { get; set; }


  public DateTime Data_inicio { get; set; }


  public DateTime Data_fim { get; set; }


  public decimal Orcamento { get; set; }


  public string Cliente { get; set; }


  public string Patrocinador { get; set; }


  public string Objetivo { get; set; }

  //ManyToOne
  public Projeto? ProjetoPai { get; set; }
  public Guid? projetoId {get; set; }

  //OneToMany
  public ICollection<Projeto>? Projetos { get; set;}

  //OneToMany
  public ICollection<Time>? Times { get; set;}

  //ManyToOne
  public Categoria? Categoria { get; set; }
  public Guid? categoriaId {get; set; }


  public ProjetoStatus projetostatus { get; set; }


}
}
